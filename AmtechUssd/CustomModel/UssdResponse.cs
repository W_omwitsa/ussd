﻿namespace AmtechUssd.CustomModel
{
	public class UssdResponse
	{
		public string SessionId { get; set; }
		public string ServiceCode { get; set; }
		public string PhoneNumber { get; set; }
		public string Text { get; set; }
		public string NetworkCode { get; set; }
		public string FacilitatorCode { get; set; }
		public string FacilitatorName { get; set; }
	}
}