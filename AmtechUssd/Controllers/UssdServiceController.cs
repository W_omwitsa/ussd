﻿using AmtechUssd.CustomModel;
using AmtechUssd.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace AmtechUssd.Controllers
{
	[RoutePrefix("application/services")]
	public class UssdServiceController : ApiController
	{
		ACSEntities dbContext;
		public UssdServiceController()
		{
			dbContext = new ACSEntities();
		}

		[Route("ussdservice")] 
		[HttpPost, ActionName("ussdservice")]
		public HttpResponseMessage HttpResponseMessage([FromBody] UssdResponse ussdResponse)
		{
			ussdResponse.Text = ussdResponse?.Text ?? "";
			ussdResponse.Text = ussdResponse.Text.Length == 2 ? ussdResponse.Text.Remove(0, 2) : ussdResponse.Text;
			var arrResponse = ussdResponse.Text.Split('*');
			if(arrResponse[0].Equals("20"))
			{
				arrResponse = arrResponse.Skip(1).ToArray();
				ussdResponse.Text = string.Join("*", arrResponse);
			}
			
			ussdResponse.SessionId = ussdResponse?.SessionId ?? "";
			HttpResponseMessage responseMessage;
			string response = UssdOperation(ussdResponse).Message;

			responseMessage = Request.CreateResponse(HttpStatusCode.Created, response);
			responseMessage.Content = new StringContent(response, Encoding.UTF8, "text/plain");
			return responseMessage;
		}

		private ReturnData UssdOperation(UssdResponse ussdResponse)
		{
			try
			{
				ussdResponse.Text = ussdResponse?.Text ?? "";

				if (!string.IsNullOrEmpty(ussdResponse.Text) && ussdResponse.Text.Substring(0, 1).Equals("*"))
					ussdResponse.Text = ussdResponse.Text.Remove(0, 1);

				var ussdDetails = dbContext.USSDDetails.FirstOrDefault(s => s.SessionId.ToUpper().Equals(ussdResponse.SessionId.ToUpper()));
				var member = dbContext.members.FirstOrDefault(m => m.IDNo.ToUpper().Equals(ussdResponse.Text.ToUpper()));
				if (member != null)
				{
					member.IDNo = member?.IDNo ?? "";
					member.IDNo = member.IDNo.Trim();
					member.Surname = member?.Surname ?? "";
				}
				
				if (ussdResponse.Text.Equals("", StringComparison.Ordinal))
				{
					if (ussdDetails == null)
					{
						dbContext.USSDDetails.Add(new USSDDetail
						{
							SessionId = ussdResponse.SessionId,
						});

						dbContext.SaveChanges();
					}

					return new ReturnData
					{
						Message = "CON Please enter Your Id Number\n"
					};
				}
				else if (ussdResponse.Text.ToUpper().Equals($"{member?.IDNo?.ToUpper() ?? ""}", StringComparison.Ordinal))
				{
					ussdDetails.NationalId = ussdResponse?.Text?.ToUpper() ?? "";
					var message = $"CON Dear {member.Surname}, Please enter your pin to continue \n";
					if (string.IsNullOrEmpty(member.UssdPin))
					{
						Random random = new Random();
						var pin = random.Next(1000, 9999);
						member.UssdPin = pin.ToString();
						message = $"CON Enter this default pin:- {member.UssdPin} to continue. Kindly change it";
					}
					
					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = message
					};
				}
				else if (string.IsNullOrEmpty(ussdDetails.NationalId))
				{
					var client = dbContext.Clients.FirstOrDefault(c => c.IdNo.ToUpper().Equals(ussdResponse.Text.ToUpper()));
					if (client == null)
						return new ReturnData
						{
                            Message = "END Sorry we could not find your details. Kindly visit your nearest branch to complete sacco registration \n"
                        };

					ussdDetails.NationalId = ussdResponse?.Text?.ToUpper() ?? "";
					dbContext.SaveChanges();
					var miniResponse = "CON Dear Customer, You are not registered for this service\n";
					miniResponse += "Send 1 to Register\n";
					return new ReturnData
					{
						Message = miniResponse
					};
				}
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*1", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Please enter your sacco code\n"
					};
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*1*"))
				{
					ussdResponse.Text = ussdResponse?.Text ?? "";
					var arrResponse = ussdResponse.Text.Split('*');
					ussdDetails.SaccoCode = arrResponse[2];
					var client = dbContext.Clients.FirstOrDefault(c => c.IdNo.ToUpper().Equals(ussdDetails.NationalId.ToUpper()));
					client.IdNo = client?.IdNo ?? "";
					if (!dbContext.members.Any(m => m.IDNo.ToUpper().Equals(client.IdNo.ToUpper())))
					{
						member = new member
						{
							FirstName = client.Surname,
							Secondname = client.OtherNames,
							IDNo = client.IdNo.ToUpper(),
							SaccoCode = ussdDetails.SaccoCode,
							phonenumber = client.PhoneNo,
						};

						dbContext.members.Add(member);
						dbContext.SaveChanges();
					}

					return new ReturnData
					{
						Message = $"END Dear {client.Surname}, welcome to ACS. Your pin will be sent by SMS\n"
					};
				}
				else if (string.IsNullOrEmpty(ussdDetails.Pin))
				{
					var arrResponse = ussdResponse.Text.Split('*');
					ussdDetails.Pin = arrResponse[1];
					member = dbContext.members.FirstOrDefault(m => m.IDNo.ToUpper().Equals(ussdDetails.NationalId.ToUpper()));
					if (!member.UssdPin.ToUpper().Equals(ussdDetails.Pin.ToUpper()))
						return new ReturnData
						{
							Message = "END Sorry, invalid pin"
						};

					dbContext.SaveChanges();
					var miniResponse = "CON Please select a service\n";
					miniResponse += "1. Balance\n";
					miniResponse += "2. Withdraw\n";
					miniResponse += "3. Deposit\n";
					miniResponse += "4. Credit limit\n";
					miniResponse += "5. Apply for loan\n";
					miniResponse += "6. Repay loan\n";
					miniResponse += "7. Change Pin\n";
					//miniResponse += "8. Buy airtime\n";

					return new ReturnData
					{
						Message = miniResponse
					};
				}
				// Show balance
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*1", StringComparison.Ordinal))
				{
					var response = GetClient(ussdDetails);
					if (!response.Success)
						return response;

					string sno = response.Data?.SNO ?? "";
					DateTime LastMonthLastDate = DateTime.Today.AddDays(0 - DateTime.Today.Day);
					DateTime LastMonthFirstDate = LastMonthLastDate.AddDays(1 - LastMonthLastDate.Day);
					var transaction = dbContext.d_MACS.Where(d => d.SNo.ToUpper().Equals(sno.ToUpper()) && d.TransDate >= LastMonthFirstDate && d.TransDate <= LastMonthLastDate)
									   .Select(d => d.PAmount).Sum();
					var deposits = dbContext.Deposits.Where(d => d.TransDate >= LastMonthFirstDate && d.TransDate <= LastMonthLastDate)
						.Select(d => d.Amount).Sum();
					var withdrawals = dbContext.Withdrawals.Where(d => d.TransDate >= LastMonthFirstDate && d.TransDate <= LastMonthLastDate)
						.Select(d => d.Amount).Sum();

					var balance = transaction + deposits - withdrawals;
					return new ReturnData
					{
						Message = $"END Your account balance is: {balance}"
					};
				}
				// withdrawals
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*2", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Please enter amount"
					};
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*2*"))
				{
					var response = GetClient(ussdDetails);
					if (!response.Success)
						return response;

					var arrResponse = ussdResponse.Text.Split('*');
					decimal.TryParse(arrResponse[3], out decimal withdrawalAmount);
					dbContext.Withdrawals.Add(new Withdrawal
					{
						SNO = response.Data.SNO,
						Amount = withdrawalAmount,
						TransDate = DateTime.UtcNow
					});

					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = $"END You have withdrawn KES {withdrawalAmount}"
					};
				}
				//deposit
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*3", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Please enter amount"
					};
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*3*"))
				{
					var response = GetClient(ussdDetails);
					if (!response.Success)
						return response;

					var arrResponse = ussdResponse.Text.Split('*');
					decimal.TryParse(arrResponse[3], out decimal depositedAmount);
					dbContext.Deposits.Add(new Deposit {
						SNO = response.Data.SNO,
						Amount = depositedAmount,
						TransDate = DateTime.UtcNow
					});

					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = $"END KES {depositedAmount} has been deposited to your account"
					};
				}
				// Credit limit selected
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*4", StringComparison.Ordinal))
				{
					var response = GetClient(ussdDetails);
					if (!response.Success)
						return response;

					var supplierNo = $"{response.Data.SNO}";
					var creditLimit = dbContext.CredLimits.FirstOrDefault(l => l.Sno.ToUpper().Equals(supplierNo.ToUpper()));
					decimal.TryParse(creditLimit?.CreditLimit.ToString() ?? "", out decimal limit);

					var miniResponse = $"CON Dear {response.Data.Surname} You can take up to KES {limit}, \n";
					miniResponse += "Send 1 to Apply\n";
					return new ReturnData
					{
						Message = miniResponse
					};
				}
				// loan Application After viewing Loan limit
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*4*1", StringComparison.Ordinal))
				{
					return new ReturnData
					{
						Message = "CON Please enter the amount you want to apply for"
					};
				}
				// Loan Application direct
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*5", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Please enter the amount you want to apply for"
					};
				else if (string.IsNullOrEmpty(ussdDetails.Amount)
					&& (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*5*")
					|| ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*4*1*")))
				{
					var amount = "";
					var arrResponse = ussdResponse.Text.Split('*');
					if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*4*1*"))
						amount = arrResponse[4];
					if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*5*"))
						amount = arrResponse[3];

					var response = GetClient(ussdDetails);
					if (!response.Success)
						return response;

					var supplierNo = $"{response.Data.SNO}";
					var creditLimit = dbContext.CredLimits.FirstOrDefault(l => l.Sno.ToUpper().Equals(supplierNo.ToUpper()));
					decimal.TryParse(creditLimit?.CreditLimit.ToString() ?? "", out decimal limit);
					decimal.TryParse(amount, out decimal appliedAmount);
					if (appliedAmount > limit)
						return new ReturnData
						{
							Message = $"END Sorry, your limit is KES {limit}"
						};

					var loan = new d_supplier_deduc_Loans
					{
						SNo = long.Parse(response.Data.SNO),
						Date_Deduc = DateTime.UtcNow,
						Description = "",
						Amount = appliedAmount,
						auditid = "",
						phoneno = response.Data.PhoneNo,
						RefNo = ""
					};

					dbContext.d_supplier_deduc_Loans.Add(loan);

					ussdDetails.Amount = amount;
					dbContext.SaveChanges();

					return new ReturnData
					{
						Message = "END Thank you. Your request is being processed"
					};
				}
				// Repay loan option
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*6", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Please enter the amount"
					};
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*6*{ussdDetails?.Amount ?? "0"}*"))
				{
					var response = GetClient(ussdDetails);
					if (!response.Success)
						return response;

					decimal.TryParse(ussdDetails?.Amount ?? "", out decimal repayAmount);
					dbContext.d_supplier_deduc.Add(new d_supplier_deduc {
						SNo = response.Data.SNO,
						Date_Deduc = DateTime.UtcNow,
						Description = "",
						Amount = repayAmount
					});

					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = "END Your request is being processed"
					};
				}
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*6*"))
				{
					var arrResponse = ussdResponse.Text.Split('*');
					ussdDetails.Amount = arrResponse[3];
					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = "CON Enter Mpesa pin"
					};
				}
				// Change pin option

				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*7", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Enter old pin"
					};
				else if (string.IsNullOrEmpty(ussdDetails.OldPin)
					&& ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*7*"))
				{
					var arrResponse = ussdResponse.Text.Split('*');
					ussdDetails.OldPin = arrResponse[3];
					member = dbContext.members.FirstOrDefault(m => m.IDNo.ToUpper().Equals(ussdDetails.NationalId.ToUpper()));
					if (!member.UssdPin.ToUpper().Equals(ussdDetails.OldPin.ToUpper()))
						return new ReturnData
						{
							Message = "END Sorry, Invalid pin"
						};
					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = "CON Enter New pin"
					};
				}
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*7*{ussdDetails.OldPin.ToUpper()}*{ussdDetails?.NewPin?.ToUpper() ?? "0"}*"))
				{
					var arrResponse = ussdResponse.Text.Split('*');
					ussdDetails.ConfirmPin = arrResponse[5];
					if (!ussdDetails.ConfirmPin.Equals(ussdDetails.NewPin))
						return new ReturnData
						{
							Message = "END Sorry, Entered pin and confirm pin do not match"
						};
					member = dbContext.members.FirstOrDefault(m => m.IDNo.ToUpper().Equals(ussdDetails.NationalId.ToUpper()));
					member.UssdPin = ussdDetails?.NewPin ?? "";
					dbContext.SaveChanges();
					return new ReturnData
					{
						Message = "END You have successfully changed your pin"
					};
				}
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*7*{ussdDetails.OldPin.ToUpper()}*"))
				{
					var arrResponse = ussdResponse.Text.Split('*');
					ussdDetails.NewPin = arrResponse[4];
					dbContext.SaveChanges();

					return new ReturnData
					{
						Message = "CON Confirm new pin"
					};
				}
				// Buy airtime
				else if (ussdResponse.Text.ToUpper().Equals($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*8", StringComparison.Ordinal))
					return new ReturnData
					{
						Message = "CON Please enter amount"
					};
				else if (ussdResponse.Text.ToUpper().Contains($"{ussdDetails.NationalId.ToUpper()}*{ussdDetails.Pin.ToUpper()}*8*"))
				{
					var arrResponse = ussdResponse.Text.Split('*');
					var amount = arrResponse[3];
					return new ReturnData
					{
						Message = $"END You have successfully bought an airtime of KES. {amount} "
					};
				}
				else
				{
					return new ReturnData
					{
						Message = "END Invalid option \n"
					};
				}
			}
			catch (Exception ex)
			{
				return new ReturnData
				{
					Message = "END Sorry, An error occurred"
				};
			}
		}

		private ReturnData GetClient(USSDDetail ussdDetails)
		{
			var client = dbContext.Clients.FirstOrDefault(c => c.IdNo.ToUpper().Equals(ussdDetails.NationalId.ToUpper()));
			if (client == null)
				return new ReturnData
				{
					Success = false,
					Message = $"END Sorry, your records not found"
				};
			
			if (string.IsNullOrEmpty(client.SNO))
				return new ReturnData
				{
					Success = false,
					Message = $"END Sorry, we could not find your supplier number"
				};

			return new ReturnData
			{
				Success = true,
				Data = client
			};
		}
	}
}
