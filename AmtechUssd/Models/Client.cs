//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AmtechUssd.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Client
    {
        public long ACSCode { get; set; }
        public string MemberNo { get; set; }
        public string IdNo { get; set; }
        public string PhoneNo { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> Dob { get; set; }
        public string Location { get; set; }
        public Nullable<bool> status { get; set; }
        public string Hash_Pin { get; set; }
        public System.DateTime regdate { get; set; }
        public string OrgCode { get; set; }
        public string auditID { get; set; }
        public bool lstatus { get; set; }
        public string Coop { get; set; }
        public string AccNo { get; set; }
        public string Names { get; set; }
        public string SNO { get; set; }
    }
}
